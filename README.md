# Simple QL-500 Label Printing

![QLabels Logo](logo.png)

## What

The QL-500 connects via USB, is supported by CUPS, and all you have to
do to print to it is create a precisely dimensioned PDF (the most
common reason for getting a blinking green light instead of a printed
label is inexact dimensions.)

Still, you don't want to do full scale document composition for a
label the size of a couple of postage stamps, you just want to get a
couple of words and numbers down and slap something on the wall.

* "You'd just use a sharpie but your handwriting is bad" 
* "I want to print fifty of these counting up and quickly but I don't
   want to waste that many before I see what they look like"
* "I have a *system* and have URLs for each shelf, and want to make QR
   code labels that point to them without a lot of work."
* "I have a portable label maker and hate the keyboard *so much* that
   I'd rather just type the words on a computer and have it print them."

## Tools

For many uses, the command line tools will simply let you generate a
PDF with some really basic labeling on it:

### `ql-simple`

* `--qr=value` add a QR code to one end of the label, shrinking
      the "workspace" for the remaining words. URLs just work.
* `--outputpdf=file.pdf` put the labels in this PDF file (if the
      QL-500 is the only printer you have hooked up, `lpr file.pdf`
      will print labels immediately, even a single label, without
      waste.
* `--kind=...` specify the kind; see `--help` for the currently
      supported ones.
* `--grid` include a calibration grid.

Remaining arguments are treated as a list of words, centered on the
label (if `--qr` is given they are centered on the remainder of the
label.)

Example uses:
* `ql-simple --grid --outputpdf gt.pdf "DK-1201 29x90.3"` generate a
  descriptive grid-label, primarily for calibration and debugging
* `ql-simple --outputpdf shelf.pdf --kind DK-1203 label printing supplies`
  generate a file-folder label with three words on it.
* `ql-simple --outputpdf inventory.pdf --qr SN-99731 Bulk Item 99731`
  generate a human readable label with a scannable serial number.

### `ql-range`
* `--low=start`, `--high=end` generate labels for values from
      start to end, inclusive.
* `--outputpdf`, `--kind` same as for `ql-simple`
* `--qr=value` same except that if value includes a `%` then it is
      fed the label number for formatting; typically this will be used
      as `--qr=https://jira.example.com/browse/INV-%s` which will
      generate labels from `INV-1` to `INV-99` with the other
      defaults.  Use `%03d` instead if you want 3-digit padding.  In
      either case, point your phone at the PDF on your screen and
      *test* the links first!

The remaining arguments are a list of words as with `ql-simple` but
only one of them can contain a `%` format string.

Example uses:
* `ql-range --qr "http://example.com/machine-%02d.xml" --outputpdf mach.pdf "Machine %02d" --low 32 --high 50`
  generates numbered machine-name stickers from 32 to 50 with qr codes
  that point to a structured url (perhaps a maintenance log in a
  ticketing system.)

### `ql-batch`

* `--outputpdf`, `--kind` same as for `ql-simple`
* `--qr` is the name of a *field* in the CSV that should render as
      an optional qr code.
* `--label` is the name of a field in the CSV to render as
      one-line centered text.

Remaining arguments are the names of CSV files; they can have
different columnt layouts as long as the field name (as specified in
the first line.) See `pydoc pandas.read_csv` for more details.

Example uses:
* `ql-batch --outputpdf qb.pdf --qr url --label ticket JIRA.csv` takes
  a csv file with a ticket column and a url column, a more explicit
  version of the `ql-range` example.  In practice, you're more likely
  to want a little code to pull from your inventory system directly,
  but the `ql-batch` source should be an OK example to start from.

## Library

It's likely to be easiest to start with one of the examples and
massage it to what you want.  For example, if you have a CSV file with
what you want on the labels, just look at `ql-range` and replace
the range loop with a loop over the output of `pandas.read_csv`, or
maybe something similar with `sqlite` or `requests.get().json()`.

Ultimately we want to keep the tools and library here really simple,
but `QLCanvas` derives from `reportlab.pdfgen.canvas.Canvas` so you
*can* apply all of `reportlab`'s PDF capabilities directly to the
object and just use it as a source of dimensions.

## Other Labels

I expect to only include labels I can actually buy, but if you want to
submit one, file a gitlab issue with
* Label "name" (`DK-1201`)
* Dimensions in mm (as printed on the roll, since we want the values
  the printer believes, rather than actual measurements)
* The descriptive words (`Shipping`, `File Folder`) on the side of the
  roll
* A snapshot of the holes in the "foot" of the roll (because I'm
  curious about the mapping from switch-closures to dimension.)

## Why

I used `glabels` for this when I first got the printer, and would
recommend it if you want to visually design your labels and do various
kinds of mailmerge on them - but *not* if you want to have your own
software control or just use a terminal prompt.  (`glabels` own
fileformat is gzipped XML...)

However, when I upgraded from Debian Jessie to to Debian Stretch,
`glabels` no longer produced working output (which is *particularly*
frustrating when the only diagnostic from the printer is a single
green LED that goes from solid to blinking.)  I poked at the output
unsuccessfully for a bit, then tried generating PDF output per the
specified label sizes - and it just worked, so here we are.

## References

* [Width/Height of text in reportlab](http://two.pairlist.net/pipermail/reportlab-users/2010-January/009208.html)
* [Reportlab qrcode/barcode quickref](https://www.blog.pythonlibrary.org/2013/03/25/reportlab-how-to-create-barcodes-in-your-pdfs-with-python/)
* [Handling Orientation in Reportlab](https://www.blog.pythonlibrary.org/2014/01/03/reportlab-create-landscape-pages/)
* [Page Size in Reportlab](https://stackoverflow.com/questions/5882636/how-to-create-a-pdf-document-with-differing-page-sizes-in-reportlab-python)
