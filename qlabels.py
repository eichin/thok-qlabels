#!/usr/bin/env python

import reportlab.pdfgen.canvas
from reportlab.graphics.barcode.qr import QrCodeWidget
from reportlab.graphics.barcode.usps import POSTNET
from reportlab.graphics.shapes import Drawing 
from reportlab.graphics import renderPDF
import pandas
import sys
from reportlab.pdfbase.pdfmetrics import stringWidth # http://two.pairlist.net/pipermail/reportlab-users/20

from collections import namedtuple

# paper measurement reference
"""
+------------------------------------------------------------------------+
|     Total backing-paper area, cut-to-cut                               |
|                                                                        |
|        +------------  mmlength  -------------------------------+       |
|        |                                                       |       |
|        |    Total Paper area of the sticker (on packaging)     |       |
|        |                                                       |       |
|        |       +--------  mmprintable  -----------------+      |       |
|        |       |                                        |      |       |
|<- mmleft ----->|     Printable area of the sticker      |<- mmright -->|
|        |       |     (ink does appear in this block)    |      |       |
|        |       |                                        |      |       |
|        |<----->|<- mmunprintable                        |      |       |
|        |       |<- mmprintable ------------------------>|      |       |
|        |       |                                        |      |       |
|        |       +----------------------------------------+      |       |
|        *<-origin ----------------------------------------------+       |
+------------------------------------------------------------------------+
"""

# y origin is the bottom edge of the paper.

# Exact dimensions are important to keeping the printer from rejecting
#  the page with blinking-green...
#
# name: part number as printed on the box or bracket
# kind: what kind of label it is sold as
# mmwidth: width of the *sticker*, not the tape, measured perpendicular to the roll
# mmlength: length of the *sticker*, in the direction of travel.
#
#  The above are printed on the packaging but confirmed; the next three are hand-measured
# mmleft: offset from cut line of paper to first printed pixel
# mmright: offset from last printed pixel to ending cut line
# mmprintable: width that gets printed; mmleft + mmprintable + mmright is the length of the backing paper
# mmunprintable: edge of sticker to ink start
#
# The coordinate origin (*not* where ink starts) is the edge of the sticker itself.

# My first 1202 roll actually stays "Standard Address" but online photos
#  say "Shipping" which makes a lot more sense.
# 1204 says also "Standard Address" but they are really return-address sized.

QLabel = namedtuple("QLabel", "name kind mmwidth mmlength mmleft mmright mmprintable mmunprintable")

DKLabels = [
    QLabel("DK-1201", "Standard Address",
           29, 90.3,  4.25, 6.5,  85.75, 2.25),
    QLabel("DK-1202", "Shipping",
           62, 100.0, 6.0,  6.5,  94.0,  3.0),
    QLabel("DK-1203", "File Folder", 
           17, 87.1,  4.65, 6.25, 83.5,  3.45),
    QLabel("DK-11221", "White Paper Tape", # 28.75 sticker-to-sticker; 31.8 width of backing (notch 2)
           23,   23,  8.25, 4.65, 19.75, 3.45), # ink starts 3mm up from paper edge/origin.
    QLabel("DK-1204", "Return Address",    # 60 sticker-to-sticker; 19.3 width of backing (notch 1)
           17,   54,  5.25, 6.25,  49.1, 3.45), # ink starts 0.8mm up from paper edge/origin
    QLabel("DK-11209", "Rotate Address",    # 35 sticker-to-sticker; 65.1 width of backing (notch 5)
           62,   29,  6.0,  6.5,   23.5, 5.8), # ink starts 0.8mm up from paper edge/origin
    QLabel("DK-2113", "Clear Rim Tape",    # continuous tape; 66.5 width of backing (notch 5)
           62, 62,  0,  0,   50, 0), # ink starts __ up from paper edge/origin
    # Only ever get blinking green light...
    # also has square-cutout notches, not circular holes.
    # failed mmlength: 29, 44, 47.1, 29, 54, 23, 87.1, 90.3, 25.93, 25.4, 68
    # failed mmwidth: 61.89, 66.5, 65 (62+3 margin?), 68 (62+3+3)
    # first feed got 44ish, second got 25.93...
    QLabel("DK-4605", "Yellow Removable Paper Tape", # continuous tape; 66.8 width of backing (notch 5)
           62, 22.25,  0,  0,   50, 0), # ink starts __ up from paper edge/origin
    # black marks 10.5, cut 25.5 (same as clear)
    # failed mmlength: 10.5, 25.5, 25.75,  (- 25.5  3.25)22.25
]

def rotate_generated_pdf(path, replace=False):
    """Rotate pdf 90 degrees, if you're laying them out wide (most common.)
       writes to foo_rot.pdf, if replace is set it replaces the original name with that.
    """
    # hint from https://www.blog.pythonlibrary.org/2014/01/03/reportlab-create-landscape-pages/
    from pyPdf import PdfFileReader, PdfFileWriter
    from os import rename

    with file(path) as pdf_read:
        pdf = PdfFileReader(pdf_read)
        output = PdfFileWriter()
        for page in range(pdf.getNumPages()):
            pdf_page = pdf.getPage(page)
            pdf_page.rotateClockwise(90)
            output.addPage(pdf_page)
        outpath = path.replace(".pdf", "_rot.pdf")
        # write lazy-loads from pdf_read, so keep it open
        with file(outpath, "wb") as pdf_write:
            output.write(pdf_write)
    if replace:
        rename(outpath, path)

class QLCanvas(reportlab.pdfgen.canvas.Canvas):
    """Canvas pre-configured for QL-500 labels, with some helpers"""
    Landscape = "Landscape"
    Portrait = "Portrait"
    Large = "Large"
    Small = "Small"
    pts_per_mm = 2.835

    def __init__(self, labelname, pdfpath, orientation=Landscape):
        """Initialize the underlying canvas to write to pdfpath, with dimensions 
        and orientation (defaults to QLCanvas.Landscape) for the given label"""
        # do the label first, don't set up a canvas (possibly writing a file) if we're givine up
        if orientation not in [self.Landscape, self.Portrait]:
            raise ValueError("Invalid orientation {}".format(orientation))
        for dklabel in DKLabels:
            if dklabel.name == labelname:
                self.dklabel = dklabel
                break
        else:
            raise ValueError("Unknown label kind {}".format(labelname))
        # reportlab objects aren't new-style
        # super(QLCanvas, self).__init__(pdfpath)
        reportlab.pdfgen.canvas.Canvas.__init__(self, pdfpath)
        if orientation == self.Landscape:
            self.setPageSize((self.pts_per_mm*dklabel.mmlength, self.pts_per_mm*dklabel.mmwidth))
        elif orientation == self.Portrait:
            self.setPageSize((self.pts_per_mm*dklabel.mmwidth, self.pts_per_mm*dklabel.mmlength))
        self.orientation = orientation
        self.qr_carveout_w = self.pts_per_mm*dklabel.mmleft
        self.qr_carveout_h = 0
        self.carveout_left = self.pts_per_mm*dklabel.mmunprintable
        self.fontsize = 24
        self.fontname = "Helvetica"
        self.linecount = 1
        self.lineindex = 0.5
        self.centered = True
        

    def add_qrcode(self, value):
        """Add a qrcode on the right (Landscape) or bottom (Portrait), scaled to fit.
           Run this first, and then later text operations will center on the remainder
           of the label."""
        # but do something more clever for the big Shipping ones...
        pw, ph = self._pagesize
        q = QrCodeWidget(value)
        q.setProperties(dict(barHeight=ph, barWidth=ph))
        qx, qy, qw, qh = q.getBounds()
        d = Drawing(qw, qh)
        d.add(q)
        renderPDF.draw(d, self, pw - qw, (ph-qh)/2, showBoundary=True)
        # TODO: other orientation
        self.qr_carveout_w = qw

    def add_postnet(self, value):
        """Add a postnet code along the bottom (Landscape.)"""
        q = POSTNET(value)
        right_edge = (self.dklabel.mmunprintable + self.dklabel.mmprintable) * self.pts_per_mm
        q.drawOn(self, right_edge-q.width, 1*self.pts_per_mm)
        self.qr_carveout_h = (self.pts_per_mm + q.barHeight) * 1.2

    def set_size(self, size):
        if size == self.Large:
            self.fontsize = 36
        elif size == self.Small:
            self.fontsize = 12
        else:
            raise ValueError("size {} not .Large, .Small".format(size))

    def set_lines(self, linecount):
        self.linecount = linecount
        self.lineindex = self.linecount - 0.5
        # calculate spacing, reject overflow?

    def set_flushleft(self):
        self.centered = False

    def add_line(self, S):
        from reportlab.pdfbase.pdfmetrics import stringWidth, getAscent
        self.setFont(self.fontname, self.fontsize)
        words_width = stringWidth(S, self._fontname, self._fontsize)
        pw, ph = self._pagesize
        lch = (ph-self.qr_carveout_h)/self.linecount # line center height
        fh = getAscent(self._fontname, self._fontsize)
        self.drawString(self.carveout_left + 
                        (((pw-self.carveout_left-self.qr_carveout_w)/2 - words_width/2) 
                         if self.centered else 0),
                        self.qr_carveout_h + self.lineindex*lch - fh/2, 
                        S)
        self.lineindex -= 1
        if self.lineindex < 0:
            # ql-batch and ql-range are simpler if the lines just wrap around
            self.lineindex += self.linecount

    def finish(self):
        """we're done, know the filename, write it out and maybe fix it"""
        self.save()
        if self.orientation == self.Landscape:
            rotate_generated_pdf(self._filename, replace=True)

    def grid(self, spacing_mm, line_thickness, x_offset, y_offset):
        """General grid (build calibration marks out of this)"""
        pw, ph = self._pagesize
        spacing = self.pts_per_mm * spacing_mm
        self.setLineWidth(line_thickness)
        for x in range(-2, int(pw/spacing)+2):
            self.line(spacing*x, 0, spacing*x, ph)
        for y in range(-2, int(ph/spacing)+2):
            self.line(0, spacing*y, pw, spacing*y)
