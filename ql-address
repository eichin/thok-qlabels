#!/usr/bin/env python

"""Print address labels - all lines, if last line matches it's moved up and barcoded."""

import qlabels
import sys
import click
import re

goodzip = re.compile("^\d{5}-\d{4}$")

@click.command()
@click.option("--big/--small", help="Big (DK-1202) or small (DK-1201) labels")
@click.option("--outputpdf", help="pdf output", type=click.Path(), required=True)
def main(big, outputpdf):
    assert outputpdf.endswith(".pdf"), outputpdf
    if big:
        kind = "DK-1202"
    else:
        kind = "DK-1201"
    ql = qlabels.QLCanvas(kind, outputpdf)
    ql.set_flushleft()
    if not big:
        ql.set_size(ql.Small)
    lines = [line.rstrip("\n") for line in sys.stdin.readlines()]
    if goodzip.match(lines[-1].strip()):
        zipline = lines.pop()
        lines[-1] = lines[-1] + " " + zipline
        ql.add_postnet(zipline)
        
    # Both sizes, the line spacing looks good with 5 lines
    lines.extend([""] * max((5 - len(lines)), 0))

    ql.set_lines(len(lines))
    for line in lines:
        ql.add_line(line)
    ql.showPage()
    ql.finish()

if __name__ == "__main__":
    main()

